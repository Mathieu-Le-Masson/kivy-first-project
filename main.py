from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.widget import Widget
from kivy.clock import Clock
from kivy.uix.gridlayout import GridLayout
from kivy.uix.button import Button


class ColorisationWidget(Widget):
    pass


class ColorisationApp(App):

    def build(self):
        layout = GridLayout(cols=2)
        layout2 = GridLayout(rows=2)
        layout3 = GridLayout(rows=6)

        layout.add_widget(layout2, layout3)

        return layout


if __name__ == '__main__':
    ColorisationApp().run()
